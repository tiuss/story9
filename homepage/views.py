from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.views.generic import DetailView, ListView
from django.views.generic.edit import UpdateView
from django.urls import reverse
from django.urls import reverse_lazy
# from ems.decorators import admin_hr_required, admin_only

# Create your views here.
def user_login(request):
    context = {}
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user:
            login(request, user)
            return HttpResponseRedirect(reverse('success'))
        else:
            context["error"] = "MASUKAN TIDAK VALID !!"
            return render(request, "login.html", context)
    else:
        return render(request, "login.html", context)

def success(request):
    context = {}
    context['user'] = request.user
    return render(request, "success.html", context)

def user_logout(request):
    if request.method == "POST":
        logout(request)
        return HttpResponseRedirect(reverse('user_login'))
