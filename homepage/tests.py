from django.test import TestCase,Client
from django.urls import resolve
from django.http import HttpRequest
from .views import user_login, success, user_logout
import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from django.contrib.auth.models import User


# Create your tests here.
class Story9UnitTest(TestCase):

    def test_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)
    
    def test_url_success_is_exist(self):
        response = Client().get('/success/')
        self.assertEqual(response.status_code,200)

    def test_using_user_login_func(self):
        found = resolve('/')
        self.assertEqual(found.func, user_login)

    def test_success_login(self):
        user = User.objects.create_user('Titus_Dirgantoro', 'titusdirgantoro@gmail.com','titusdirgantoro')
        response = Client().post('/login/',{'username':'Titus_Dirgantoro', 'password' : 'titusdirgantoro'})
        self.assertEqual(response.status_code,302)
    
    def test_failure_login(self):
        user = User.objects.create_user('Titus_Dirgantoro', 'titusdirgantoro@gmail.com','titusdirgantoro')
        response = Client().post('/login/',{'username':'Titus_Dirgantoro', 'password' : 'titusdirgantori'})
        self.assertEqual(response.status_code,200)
    
    def test_can_logout(self):
        response = Client().post('/logout/')
        self.assertEqual(response.status_code, 302)
